package com.example.supi.scorekeeper;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    int scoreTeam1 = 0;//variable declaration for goal of team1
    int scoreTeam2 = 0; //variable declaration for goal of team 2

    int foulTeam1 = 0; // variable declaration for foul of team1
    int foulTeam2 = 0; // variable declaration for foul of team2



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        displayForTeam1(0); //output declaration
        displayForTeam2(0); // output declaration
    }


    /** score of goal for team1  **/
    public void addtwoForTeam1(View v){
        scoreTeam1 = scoreTeam1 + 2;
        displayForTeam1(scoreTeam1);
    }


     /** score of foul for team1**/
    public void addoneForTeam1(View v) {
        foulTeam1 = foulTeam1 + 1;
        displayFoulForTeam1(foulTeam1);
    }


     /** displaying team1 goal **/
    public void displayForTeam1(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_1_score);
        scoreView.setText(String.valueOf(score));
    }


     /** displaying team2 foul **/
    public void displayFoulForTeam1(int score){
        TextView scoreView = (TextView) findViewById(R.id.foul_1_score);
        scoreView.setText(String.valueOf(score));

    }

  /** score of goal for team2 **/
    public void addtwoforTeam2(View v) {
        scoreTeam2 = scoreTeam2 + 2 ;
        displayForTeam2(scoreTeam2);
    }

    /** foul score for team2 **/
    public void addoneForTeam2(View v){
        foulTeam2 = foulTeam2 + 1;
        displayFoulForTeam2(foulTeam2);
    }


   /** display goal score **/
    public void displayForTeam2(int score){
        TextView scoreView = (TextView) findViewById(R.id.team_2_score);
        scoreView.setText(String.valueOf(score));

    }

     /** display foul score **/
    public void displayFoulForTeam2(int score){
        TextView scoreView = (TextView) findViewById(R.id.foul_2_score);
        scoreView.setText(String.valueOf(score));

    }

    /** reset button **/
    public void Resetscore(View v) {
        scoreTeam1 = 0;
        scoreTeam2 = 0;
        foulTeam1 = 0;
        foulTeam2 = 0;
        displayForTeam1(scoreTeam1);
        displayForTeam2(scoreTeam2);
        displayFoulForTeam1(foulTeam1);
        displayFoulForTeam2(foulTeam2);

    }
}

